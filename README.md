## Home Lab Reflow Plate

Soldering circuits by hand is a PITA and, while it's totally viable for single boards etc, reflow assembly is a truly blissful step away from the banal. 

So, yrs truly is here to spec out a reflow system. In the open source these are commonly done with modified toaster ovens, but I'm attracted to the 'hot plate' method: i.e. Skillet Reflow. Here, we (one sided only) just put boards on a hot plate, and run that plate through a temperature cycle. Since boards are mostly conductive and parts live on the surface, the local plate heating seems to make a lot of sense. 

[Here's a video of the principle](https://www.youtube.com/watch?v=dJI07PjZbdk).

I think the move is to use a Silicon Heating Pad (commonly used in 3D Printer beds), which are available up to high wattages (~1350 in one I am checking now) but to calculate this properly I need wattage / sqm: area power. And to calculate that against power req. to bring aluminum plate of whatever thickness up to temperature... at some specified rate, which (?) I think is some simple ish spreadsheet maths. 

Unless things look like they come close, I will probably do this without looking at losses from i.e. dissipation into free air, but this will be a big hole. I will insulate the thing, but not exceptionally well. 

### Calculating Power Requirement

OK, I need to know, for some set thickness of aluminum, how much power I need to raise its temperature fast enough to reflow properly.

- Aluminum has a specific heat capacity of 0.9 J/gK
    - joules per gram-degree
    - that's how many joules needed to raise one gram by one degree c 
    - so, given watts (joules/s), I can calculate a rate deg/s 
- From temperature curve in Solder Paste SMD291AX250T5-ND datasheet, in its steepest ascent, wants 85 deg rise over 90s, for 0.94 deg/s: nicely rounds to 1deg/s. 
- Aluminum has a density of 2.7g/cm^3

I've made a little spreadsheet. I've DOF in thickness and can set Watts / Square Meter by the parts I can find. 

[The Spreadsheet](2020-07-08_reflow-plate-specfest.xlsx)

I'd like for the plate to be around ~ 1/4" thick, just to be substantial enough to stay pretty flat during the process. If I want to get there, I need to target around 16kW/sqm. Since I'm not counting losses, of which there is a great deal, I should adjust up to aim closer to 20kW/sqm. With 1500W out of the wall, I can only do 0.075 sqm, or roughly 275mm square.

Finding the right part is just a trick... Around the minimum size I'd like to build this is 150x150mm, which fits a 3x3 grid of NEMA17 drivers. 

`MPHKH-150-150-V100-W500` Misumi $116 Plate Heater 22kW/sqm 

[265x405mm 1350W](https://www.ebay.com/itm/10-X-16-265-X-405mm-1350W-Flexible-Silicone-Heater-Autoclave-Tank-Heating-Pad/133145657435?hash=item1f0019805b:g:9vsAAOSwyJVdWWFd) 

It looks like the Misumi plate is about as best as it'd get. I can knock down my thickness desires and settle for 3/16" (almost 5mm). However, looking at 'the market' for plate heaters, I'd say the Misumi part is, although expensive, the best way to do good reflow. I'll pick that, and get a matching 6x6" ALU plate. The thru holes (nice) on the plate are 6mm, so I'll fit M5 legs and washers to clamp the plate down, with lock washers. 

### Parts

To control this, I'm going to pick up [one of these](https://www.tindie.com/products/seonr/reflow-master/) projects. 

The heating plate of choice is a Misumi component, 

`MPHKH-150-150-V100-W500` Misumi $116 Plate Heater 22kW/sqm 

I need an SSR, [SSRx3](https://www.amazon.com/MYSWEETY-SSR-40DA-Single-Semi-Conductor-24-380V/dp/B073B4R4LS/)

Various hardware:

- 6x6" 0.190" Cold Rolled Alu Sheet 89015K275
- M5 SHCS x30 91292A192
- M5 SHCS x10 91292A124
- M5 Nuts (not nylocks) 91828A241
- M5 Lock Washers 92148A170
- M5 Washers 93475A240

For the enclosure, I'll want some rigid insulation and reflective tape. Making a box with this will be easy, just cut & tape together, nothing fancy. 

- Hot Tape 76035A66
- [Reflective Rigid Insulation](https://www.homedepot.com/p/SilveRboard-24-in-x-1-in-x-48-in-R5-Radiant-Acoustic-Insulation-Kit-STC-19-5-Piece-SB12AKIT/205975329) (unfortunately, this is a 5-pack)

## Assembly, Test

OK, I went through a few plate thicknesses - definitely the move is to go as thin as possible while still retaining some flatness. At the moment I'm working with 1/8" thick 6061, but could probably go thinner. 

I mounted the reflow control bits in a little chassis, and just drilled & tapped the plate to mount the heating element underneath. The four corners have longer M5 BHCS, that serve as tiny feet for the plate as well. 

![top](2020-11-10_reflow-top.jpg)
![bottom](2020-11-10_reflow-bottom.jpg)

I added a new profile to the ReflowMaster code, you do this just by adding some lines to the source and re-flashing the board.

```cpp
float baseGraphX[5] = { 1,  180,  240,  350,  400 }; // time
float baseGraphY[5] = { 25, 150,  175,  235,  25  }; // value

solderPaste[0] = ReflowGraph( "HOTPLATE", "Ramp to 225 Surface Temp", 225, baseGraphX, baseGraphY, ELEMENTS(baseGraphX), 360, 450 ); // last two: fan time, off time 

float baseGraphX1[4] = { 1,  100,  210,  250 }; // time
float baseGraphY1[4] = { 25, 175,  235,  25  }; // value

solderPaste[1] = ReflowGraph( "HP WARMED", "Fast Early Ramp", 225, baseGraphX1, baseGraphY1, ELEMENTS(baseGraphX1), 260, 300 );
```

![melt-01](2020-11-10_reflow-melt-01.mp4)
![melt-02](2020-11-10_reflow-melt-02.mp4)